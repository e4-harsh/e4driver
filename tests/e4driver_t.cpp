#include "e4driver/e4driver.hpp"
#include "catch2/catch.hpp"

using namespace e4driver;

TEST_CASE("Rest", "[simple]")
{
    rest_client_t cli;
    // Setup a custom resolver to duck DNS overhead
    cli.add_resolver("httpbin.org", 443, "107.23.135.58");
    // Build the request
    cli.request("{ \"hello\": \"world\" }", 24, "https://httpbin.org/post");
    // Poll for send and receive
    while (cli.poll() != 0)
        ;
    std::array<char, 1024> resp = {'\0'};
    cli.get_response(resp);
    std::cerr << resp.data() << std::endl;
}