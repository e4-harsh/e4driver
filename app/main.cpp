#include <mimalloc.h>
// #include <mimalloc-new-delete.h>

#include "e4driver/e4driver.hpp"

#include <iostream>
#include <filesystem>
#include <chrono>
#include <random>
#include <iostream>
#include <fstream>
#include <unistd.h>
#include <getopt.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <ext/toml.hpp>
#include <ext/lyra.hpp>
#include <ext/aixlog.hpp>

using namespace e4driver;

#include "litelog.h"

#define RINGBUFFER_SIZE 128

#include "packet.h"
#include "generator.inl"
#include "dispatcher.inl"


std::string initialize_store(const std::string& store_path)
{
    auto now = std::chrono::system_clock::now();
    std::time_t time = std::chrono::system_clock::to_time_t(now);
    struct std::tm* timeinfo;
    char dateBuffer[9];
    timeinfo = std::localtime(&time);
    std::strftime(dateBuffer, sizeof(dateBuffer), "%Y%m%d", timeinfo);

    std::string dated_path = store_path + "/" + dateBuffer;

    try {
        std::filesystem::create_directories(dated_path + "/store");
    } catch (const std::exception& e) {
        std::cerr << "Error creating subdirectory: " << e.what() << std::endl;
    }

    return dated_path;
}

void daemonize(const std::string &pid_file, const std::string &store_path)
{
  // Change the current working directory
  if (chdir(store_path.c_str()) != 0) {
    std::cerr << "Couldn't change working directory to store path " << store_path;
    std::cerr << std::endl;
    std::exit(-1);
  }

  pid_t pid = fork();

  if (pid < 0)
  {
    std::cerr << "Failed to fork the process." << std::endl;
    exit(1);
  }

  if (pid > 0)
  {
    exit(0);
  }

  setsid();

  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);

  std::ofstream ofs(pid_file);
  if (ofs.is_open())
  {
    ofs << getpid();
    ofs.close();
  }
  else
  {
    std::cerr << "Failed to create the PID file." << std::endl;
    exit(1);
  }
}

int main(int argc, char *argv[])
{
  int8_t log_verbosity = 3;
  std::string config_file;
  bool as_daemon = false;
  std::string store_path = "/tmp/tradeserver";
  std::string pid_file = argv[0];
  pid_file += ".pid";
  bool show_help = false;

  auto cli = lyra::cli()
      | lyra::opt(
            [&](bool){ log_verbosity += 1; })  
            ["-v"].cardinality(0, 3)["--verbosity"]
            ("Logging verbosity")
      | lyra::opt(config_file, "config_file")
            ["-c"]["--config"]
            ("YAML configuration")
      | lyra::opt(store_path, "store_path")
            ["-s"]["--store"]
            ("Logging and state storage path")
      | lyra::opt(as_daemon)
            ["-d"]["--daemonize"]
            ("Run as a daemon process")
      | lyra::help(show_help).description("e4 tradeserver");

  auto result = cli.parse({argc, argv});

  if (!result || config_file.empty() || show_help) {
    std::cerr << "Command line error [" << result.message() << "]\n\n";
    std::cerr << cli << std::endl;
    return -1;
  }

  // Make our store path if it doesnt exist
  std::string log_path = initialize_store(store_path);
  auto sev = static_cast<AixLog::Severity>(6 - log_verbosity);

  if (as_daemon) {
    daemonize(pid_file, store_path);
    // Running in the background. Only make a file logger
    AixLog::Log::init<AixLog::SinkFile>(sev, log_path + "/tradeserver.log");
  } else {
    // Running on console only. Using stdout logger alone
    AixLog::Log::init<AixLog::SinkCout>(sev);
  }

  LOG(NOTICE) << "Starting tradeserver. Logging @" << AixLog::to_string(sev) << "\n";

  try
  {
    LOG(INFO) << "Parsing configuration ..." << std::endl;
    auto conf = toml::parse_file(config_file);

    LOG(INFO) << "Initializing generator ..." << std::endl;
    OrderGenerator gen;
    gen.initialize(conf);
    LOG(INFO) << "Initializing dispatcher ..." << std::endl;
    OrderDispatcher dispatcher(gen.queues());
    dispatcher.initialize(conf);

    gen.start();
    dispatcher.start();

    // Wait and cleanup
    gen.join();
    dispatcher.join();
  }
  catch (std::exception &x)
  {
    LOG(INFO) << "Abnormal exit: " << x.what() << std::endl;
    return -1;
  }

  LOG(INFO) << "Exiting normally" << std::endl;
  return 0;
}
