#include <array>
#include <ctime>
#include <type_traits>

using buffer_t = std::array<char, 1024>;

class packet_t : public buffer_t
{
public:
    using tspec_t = struct timespec;
    enum state_t
    {
        CREATED = 0,
        ENCODED,
        ENQUEUED,
        DEQUEUED,
        SENT,
        RECEIVED,
        MAX_STATES,
    };

    template <state_t state>
    void clock()
    {
        clock_gettime(CLOCK_REALTIME, &m_timestamps[state]);
    }

    template <state_t from, state_t to>
    uint64_t get_timediff()
    {
        timespec result;
        auto &a = m_timestamps[from];
        auto &b = m_timestamps[to];

        if (a.tv_sec > b.tv_sec || (a.tv_sec == b.tv_sec && a.tv_nsec > b.tv_nsec))
        {
            std::swap(a, b);
        }

        if (b.tv_nsec < a.tv_nsec)
        {
            result.tv_sec = b.tv_sec - a.tv_sec - 1;
            result.tv_nsec = 1000000000 + b.tv_nsec - a.tv_nsec;
        }
        else
        {
            result.tv_sec = b.tv_sec - a.tv_sec;
            result.tv_nsec = b.tv_nsec - a.tv_nsec;
        }

        return static_cast<uint64_t>(result.tv_sec) * 1000000000 + result.tv_nsec;
    }

    packet_t(size_t len) : m_length(len)
    {
        clock<CREATED>();
    }

    packet_t &operator=(const buffer_t &buffer)
    {
        static_cast<buffer_t &>(*this) = buffer;
        return *this;
    }

    packet_t &operator=(const std::string &str)
    {
        assert(str.length() <= buffer_t::size());
        std::copy(str.begin(), str.end(), begin());
        return *this;
    }

    const size_t length() {
        return m_length;
    }

    packet_t(const packet_t &) = delete;
    packet_t &operator=(const packet_t &) = delete;

private:
    using latency_t = std::array<tspec_t, MAX_STATES>;

    latency_t m_timestamps = {'\0'};
    size_t m_length = 0;
};