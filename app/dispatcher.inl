#include <ext/statistics.h>

class OrderDispatcher : public thread_object_t<OrderDispatcher, packet_t *, RINGBUFFER_SIZE>,
                        public litelog_t
{
private:
  int m_warmup = 0;

  using stats_t = Statistics<decltype(packet_t::tspec_t::tv_nsec)>;
  enum timing
  {
    TO_ENCODE = 0,
    TO_ENQUEUE,
    TO_DEQUEUE,
    TO_SEND,
    END_TO_END,
    MAX,
  };

  rest_client_t m_restcli;
  std::array<stats_t, timing::MAX> m_latency = {};

public:
  OrderDispatcher(channel_t &upstream_channel)
    : thread_object_t(upstream_channel)
    {}
  void initialize(auto conf);
  void run();
  void record_latency(std::unique_ptr<packet_t> packet);
};

void OrderDispatcher::record_latency(std::unique_ptr<packet_t> packet)
{
  // Ignore the first few since they form warm up outliers
  m_warmup++;
  if (m_warmup < 10)
    return;

  m_latency[TO_ENCODE].insert(packet->get_timediff<packet_t::CREATED, packet_t::ENCODED>());
  m_latency[TO_ENQUEUE].insert(packet->get_timediff<packet_t::ENCODED, packet_t::ENQUEUED>());
  m_latency[TO_DEQUEUE].insert(packet->get_timediff<packet_t::ENQUEUED, packet_t::DEQUEUED>());
  m_latency[TO_SEND].insert(packet->get_timediff<packet_t::DEQUEUED, packet_t::SENT>());
  m_latency[END_TO_END].insert(packet->get_timediff<packet_t::CREATED, packet_t::SENT>());

  auto print_counter = [&](const int COUNTER, const char *label)
  {
    if (m_latency[COUNTER].count() % 10 == 0)
    {
      info("Latency %s Mean: %ld Min: %ld Max: %ld StdDev: %ld",
           label, m_latency[COUNTER].mean(), m_latency[COUNTER].min(),
           m_latency[COUNTER].max(), m_latency[COUNTER].standardDeviation());
      m_latency[COUNTER].clear();
    }
  };

  print_counter(TO_ENCODE, "(encoding)");
  print_counter(TO_ENQUEUE, "(enc_to_queue)");
  print_counter(TO_DEQUEUE, "(enq_to_deq)");
  print_counter(TO_SEND, "(deq_to_send)");
  print_counter(END_TO_END, "(e2e)");
}

void OrderDispatcher::initialize(auto conf)
{
  if (conf.find("dispatcher") != conf.end())
  {
      const toml::table& dispatcher = *conf["dispatcher"].as_table();

      std::string source_interface = dispatcher["source_interface"].as_string()->get();
      m_restcli.add_source_interface(source_interface.c_str());

      std::string api_endpoint = dispatcher["api_endpoint"].as_string()->get();
      m_restcli.default_url(api_endpoint.c_str());

      // Note: This may not be the same for all exchanges (Binance e.g. doesnt take json input)
      m_restcli.add_default_header("Content-Type", "application/json");

      // This is added automatically when using POST 
      // m_restcli.add_default_header("Content-Type", "application/x-www-form-urlencoded");

      const toml::table& resolver = *dispatcher["resolver"].as_table();
      for (const auto& entry: resolver) {
        std::string key = entry.first.data();
        std::string val = entry.second.as_string()->get();
        m_restcli.add_resolver(key.c_str(), 443, val.c_str());
      }
  }
}

void OrderDispatcher::run()
{
  int count = 20;
  packet_t *x_msg = nullptr;

  while (true)
  {
    // Pick up messages from the inbound queue
    while (m_channel[Send].pop(x_msg))
    {
      std::unique_ptr<packet_t> exchange_msg(x_msg);
      exchange_msg->clock<packet_t::DEQUEUED>();

      // Binance needs to hit api.binance.com/api/v3/order
      // Header: host, content-type: application/x-www-form-urlencoded, accept: application/json
      //         X-MBX-APIKEY = hmac signature

      // Payload
      // symbol
      // client order id
      // side (BUY|SELL)
      // type
      // quantity
      // price
      // tif (GTC|FOK)

      m_restcli.request(exchange_msg->data(), exchange_msg->length());


      exchange_msg->clock<packet_t::SENT>();

      record_latency(std::move(exchange_msg));

      // TODO: Save exchange messages here and map them back on responses below
    }

    // Poll for messages transmission
    m_restcli.poll();

    if (m_restcli.has_data())
    {
      auto response = new packet_t(0);   // Size populated by get_response()
      m_restcli.get_response(*response); 
      while (!m_channel[Receive].push(response))
      { /* spin */
      }
    }

    m_restcli.service_sessions();
  }
}
