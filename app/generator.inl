
class OrderGenerator : public thread_object_t<OrderGenerator, packet_t*, RINGBUFFER_SIZE>,
                       public litelog_t
{
private:
  udplistener_t m_strat;
  bool m_running = true; // TODO: Trigger graceful shutdown somewhere

public:
  void run();
  void initialize(auto conf);
  packet_t* encode(buffer_t& msg, size_t len);
};

void OrderGenerator::initialize(auto conf)
{
  int port = 9000;
  try
  {
    // TODO: get better at the toml api
    if (conf.find("listener") != conf.end()){
      toml::v3::node_view p = conf["listener"]["port"];
      port = p.as_integer()->get();
    }
  }
  catch (std::exception &x)
  {
    error("Error reading config. Using default port %d", port);
  }

  if (!m_strat.initialize(port))
  {
    throw exceptions::initialization_error("Listener initialization failed", __LINE__);
  }
}

packet_t* OrderGenerator::encode(buffer_t& msg, size_t len)
{
  // This would be exchange specific payloads
  auto &encoded = *new packet_t(len);

  encoded = msg; // Saving the inbound payload as outbound data (passthrough)

  encoded.clock<packet_t::ENCODED>();

  return &encoded;
}

void OrderGenerator::run()
{
  buffer_t inbuf;

  while (m_running)
  {
    // Poll the inbound socket for data
    if (auto len = m_strat.poll(inbuf); len > 0)
    {
      trace("Inbound: %s", std::string(inbuf.data(), len).c_str());
      std::unique_ptr<packet_t> encoded(encode(inbuf, len));

      encoded->clock<packet_t::ENQUEUED>();
      while (!m_channel[Send].push(encoded.release())) { /* spin */ }
    }

    // Poll exchange incoming queue
    packet_t* x_response = {};
    while (m_channel[Receive].pop(x_response))
    {
        std::unique_ptr<packet_t> x_reply(x_response);
        trace("Reply: \n%s", x_response->data());
    }

  }
}