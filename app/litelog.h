// vim: ft=cpp smartindent tabstop=4 shiftwidth=4 expandtab

#pragma once

#include <iostream>
#include <thread>
#include <type_traits>
#include <functional>
#include <cassert>
#include <cmath>
#include <cstring>

#ifndef LOG_THRESHOLD
#define LOG_THRESHOLD 6 /* MAX */
#endif

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat-security"

class litelog_t
{
private:
    static const size_t BUFMAX = 9128;
    char m_logbuf[BUFMAX + 4] = {'\0'};
    std::string m_appname;
    using level_t = enum { FATAL,
                           ERROR,
                           WARN,
                           INFO,
                           DEBUG,
                           TRACE,
                           MAX };

    size_t severity(char *buf, uint32_t code)
    {
        static struct __vt
        {
            uint32_t num;
            uint32_t bgc;
            uint32_t fgc;
            const char *value;
        } values[] =
            {
                {FATAL, 41, 91, "FATAL"},
                {ERROR, 41, 96, "ERROR"},
                {WARN, 43, 30, "WARN"},
                {INFO, 49, 90, "INFO"},
                {DEBUG, 40, 97, "DEBUG"},
                {TRACE, 47, 30, "TRACE"},
                {MAX, 49, 97, "INVALID"},
            };
        assert(code < MAX);
#ifdef ENABLE_COLOR
        return std::snprintf(buf, 20, "\e[%d;%d;1m%5s\e[0m",
                             values[code].bgc, values[code].fgc,
                             values[code].value);
#else
        return std::snprintf(buf, 20, "%-5s", values[code].value);
#endif
    }

    static constexpr bool allow_log(unsigned lvl)
    {
        return (lvl <= LOG_THRESHOLD);
    }

    static uint64_t get_tid()
    {
        thread_local static uint64_t m_tid = 0;

        if (!m_tid) [[unlikely]]
        {
            auto tid = std::this_thread::get_id();
            static_assert(sizeof(tid) == sizeof(uint64_t),
                          "Unsupported platform/build");
            m_tid = *((uint64_t *)&tid);
        }

        return m_tid;
    }

    template <size_t N>
    size_t logheader(char (&buf)[N],
                     level_t level,
                     const std::string &title)
    {
        memset(m_logbuf, '\0', sizeof(m_logbuf));
        time_t seconds;
        struct timespec spec;
        struct tm t;
        clock_gettime(CLOCK_REALTIME, &spec);
        seconds = spec.tv_sec;
        long ms = std::round(spec.tv_nsec / 1.0e3); // Nano -> Micro

        if (ms > 999999)
        {
            seconds++;
            ms = 0;
        }

        localtime_r(&seconds, &t);
        auto offset = strftime(buf, sizeof(buf), "%Y%m%d %H%M%S.", &t);
        offset += snprintf(buf + offset, sizeof(buf) - offset, "%06ld ", ms);
        offset += severity(buf + offset, level);
        offset += snprintf(buf + offset, sizeof(buf) - offset, " %6s %lu  ",
                           title.c_str(),
                           get_tid());
        return offset;
    }

    template <level_t level, typename... Args>
    typename std::enable_if<allow_log(level)>::type
    log(Args... args)
    {
        auto len = logheader(m_logbuf, level, m_appname);
        len += snprintf(m_logbuf + len, BUFMAX - len, args...);
        m_logbuf[BUFMAX] = '\0';
        std::cerr << m_logbuf << "\n";
    }

    template <level_t level, typename... Args>
    typename std::enable_if<!allow_log(level)>::type
    log(Args... args)
    {
        // No-op
    }

public:
    litelog_t(const litelog_t &) = delete;
    litelog_t &operator=(const litelog_t &) = delete;
    litelog_t() : m_appname("e4driver")
    {
    }
    virtual ~litelog_t() {}

    void set_appname(const std::string appname)
    {
        m_appname = appname;
    }

    void set_header(const std::string hdr)
    {
        set_appname(hdr);
    }

    template <typename... Args>
    void trace(Args... args)
    {
        log<TRACE>(args...);
    }

    template <typename... Args>
    void debug(Args... args)
    {
        log<DEBUG>(args...);
    }
    template <typename... Args>
    void info(Args... args)
    {
        log<INFO>(args...);
    }
    template <typename... Args>
    void warn(Args... args)
    {
        log<WARN>(args...);
    }
    template <typename... Args>
    void error(Args... args)
    {
        log<ERROR>(args...);
    }
    template <typename... Args>
    void fatal(Args... args)
    {
        log<FATAL>(args...);
        exit(0);
    }
};

#pragma GCC diagnostic pop
