# Welcome to e4driver




# Prerequisites

Building e4driver requires the following software installed:

* A C++20-compliant compiler
* CMake `>= 3.9`
* boost::lockfree
* pistache

## Installing dependencies

```bash
sudo apt install libboost-all-dev libcurl4-openssl-dev
```

# Building e4driver

The following sequence of commands builds e4driver.
It assumes that your current working directory is the top-level directory
of the freshly cloned repository:

```
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
cmake --build .
```

The build process can be customized with the following CMake variables,
which can be set by adding `-D<var>={ON, OFF}` to the `cmake` call:

* `BUILD_TESTING`: Enable building of the test suite (default: `ON`)



# Testing e4driver

When built according to the above explanation (with `-DBUILD_TESTING=ON`),
the C++ test suite of `e4driver` can be run using
`ctest` from the build directory:

```
cd build
ctest
```


# Documentation

e4driver *should* provide a documentation.
