This code is a proof of concept for a ring buffer based dispatch and receive driver for a crypto market.

The eventual goal is to do ths following in TS:

- Replace API handlers to encode HTTP packets directly to a buffer
- Replace StartMainLoop handling to remove io_context
- Use a single inbound thread to poll UDP encode and push to an outbound SPSC queue. Followed by a pull from the inbound queue calling handleReceive
- Use a single polling thread to push outbound messages and pull inbound messages 
- You can use HTTPSession but you probably cannot use the request object. That will need to be replaced. Need to find an alternative.
