
namespace e4driver
{
    class udplistener_t
    {
    private:
        int m_port = 0;
        int m_socket = -1;

    public:
        udplistener_t() = default;

        bool initialize(int port)
        {
            m_port = port;
            m_socket = socket(AF_INET, SOCK_DGRAM, 0);
            if (m_socket < 0)
            {
                std::cerr << "Error creating socket: " << strerror(errno) << std::endl;
                return false;
            }

            if (fcntl(m_socket, F_SETFL, O_NONBLOCK) < 0)
            {
                std::cerr << "Error setting socket to non-blocking: " << strerror(errno) << std::endl;
                return false;
            }

            sockaddr_in sai;
            memset(&sai, 0, sizeof(sai));
            sai.sin_family = AF_INET;
            sai.sin_addr.s_addr = INADDR_ANY;
            sai.sin_port = htons(m_port);

            if (bind(m_socket, (struct sockaddr *)&sai, sizeof(sai)) < 0)
            {
                std::cerr << "Error binding socket: " << strerror(errno) << std::endl;
                return false;
            }

            LOG(NOTICE) << "Initialized socket for port " << m_port << std::endl;

            return true;
        }

        template <size_t BUFMAX>
        int poll(std::array<char, BUFMAX> &buffer)
        {
            sockaddr_in cli_sai;
            socklen_t addrlen = sizeof(cli_sai);

            int cumulative_length = 0;
            do
            {
                int bytes_read = recvfrom(m_socket, buffer.data() + cumulative_length, BUFMAX - cumulative_length, 0, (struct sockaddr *)&cli_sai, &addrlen);

                if (bytes_read > 0)
                {
                    LOG(TRACE) << "Received message on UDP of length(bytes) " << bytes_read << std::endl;
                    cumulative_length += bytes_read;
                    assert(cumulative_length <= BUFMAX); // Buffer overflow. Adjust your source buffer to be bigger
                }
                else if (errno == EAGAIN || errno == EWOULDBLOCK) [[likely]]
                    return cumulative_length;
                else if (bytes_read == 0)
                {
                    LOG(FATAL) << "Zero byte read. Client disconnected: " << strerror(errno) << std::endl;
                    break; // Closed by client?
                }
                else
                {
                    LOG(ERROR) << "Error receiving data: " << strerror(errno) << std::endl;
                    break;
                }
            } while (true);

            return 0;
        }

        ~udplistener_t()
        {
            if (m_socket != -1)
            {
                close(m_socket);
            }
        }
    };
}