namespace e4driver
{
    const size_t MAX_NET_BUF = 1024;
    struct reply_t : std::array<char, MAX_NET_BUF> {
        size_t len = 0;
    };

    class rest_client_t
    {
    private:
        CURL *m_curl = nullptr;
        curl_slist *m_resolv = nullptr;
        curl_slist *m_headers = nullptr;
        CURLM *m_multi_handle = nullptr;
        int m_epoll_fd = -1;

        const size_t MAX_CURL_BUF = 256;
        reply_t m_response;

        /*
        struct handle_collector_t
        {
            void operator()(CURL *handle)
            {
            if (handle)
            {
                std::cerr << "Cleaning up ptr handle" << std::endl;
                curl_easy_cleanup(handle);
            }
            }
        };
        using handle_ptr = std::unique_ptr<CURL, handle_collector_t>;
        */

    public:
        rest_client_t()
        {
            m_curl = curl_easy_init();
            m_multi_handle = curl_multi_init();
            m_resolv = curl_slist_append(nullptr, "e4driver.test.com:80:127.0.0.1");

            add_default_header("Accept", "application/json");
            m_epoll_fd = epoll_create(1);

            // Restrict to V4 (perf)
            curl_easy_setopt(m_curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);

            curl_easy_setopt(m_curl, CURLOPT_WRITEFUNCTION, &rest_client_t::http_cb);
            curl_easy_setopt(m_curl, CURLOPT_WRITEDATA, this);

            // Stick the default headers and resolver (can be overridden with add_*)
            curl_easy_setopt(m_curl, CURLOPT_HTTPHEADER, m_headers);
            curl_easy_setopt(m_curl, CURLOPT_RESOLVE, m_resolv);
            curl_easy_setopt(m_curl, CURLOPT_CUSTOMREQUEST, "POST");

            m_response.fill('\0');
        }

        ~rest_client_t()
        {
            if (m_multi_handle)
                curl_multi_cleanup(m_multi_handle);
            if (m_resolv)
                curl_slist_free_all(m_resolv);
            if (m_headers)
                curl_slist_free_all(m_headers);
            if (m_curl)
                curl_easy_cleanup(m_curl);
            m_multi_handle = m_curl = m_resolv = m_headers = nullptr;
        }

        bool has_data()
        {
            return m_response[0] != '\0';
        }

        void get_response(auto& response)
        {
            std::copy(m_response.begin(), m_response.end(), response.data());
            clear();
        }

        void clear()
        {
            m_response.fill('\0');
            m_response.len = 0;
        }

        static size_t http_cb(void *contents, size_t size, size_t nmemb, void *userp)
        {
            return static_cast<rest_client_t *>(userp)->http_response(contents, size, nmemb);
        }

        size_t http_response(void *contents, size_t size, size_t nmemb)
        {
            size_t total_size = size * nmemb;
            memcpy(m_response.data()+m_response.len, contents, total_size);
            m_response.len += total_size;
            assert(m_response.len <= MAX_NET_BUF);  // These are all static buffers. Make sure we're in range
            return total_size;
        }

        void add_default_header(std::string name, std::string value)
        {
            auto *header = new char[MAX_CURL_BUF];
            auto len = std::snprintf(header, MAX_CURL_BUF, "%s: %s", name.c_str(), value.c_str());
            assert(len <= MAX_CURL_BUF);
            m_headers = curl_slist_append(m_headers, header);
            assert(m_headers);
            curl_easy_setopt(m_curl, CURLOPT_HTTPHEADER, m_headers);
        }

        void add_resolver(const char *hostname, int port, const char *ipaddr)
        {
            auto *resolv_buf = new char[MAX_CURL_BUF];
            auto len = std::snprintf(resolv_buf, MAX_CURL_BUF, "%s:%d:%s", hostname, port, ipaddr);
            assert(len <= MAX_CURL_BUF);
            m_resolv = curl_slist_append(m_resolv, resolv_buf);
            assert(m_resolv);
            assert(m_curl);
            curl_easy_setopt(m_curl, CURLOPT_RESOLVE, m_resolv);
        }

        void add_source_interface(const char *iface)
        {
            curl_easy_setopt(m_curl, CURLOPT_INTERFACE, iface);
        }

        void default_url(const char *url)
        {
            curl_easy_setopt(m_curl, CURLOPT_URL, url);
        }

        int poll()
        {

            int transfers_pending = 0;
            int numfds = {};

            do
            {
                CURLMcode mc = curl_multi_perform(m_multi_handle, &transfers_pending);

                if (mc == CURLM_OK)
                {
                    mc = curl_multi_poll(m_multi_handle, NULL, 0, 0, &numfds);
                    assert(mc == CURLM_OK);
                }
                else
                {
                    LOG(ERROR) << "curl_multi_perform() failed: " << curl_multi_strerror(mc) << std::endl;
                }

            } while (transfers_pending);

            return gc();
        }

        int gc()
        {
            // Garbage collect handles
            struct CURLMsg *m = nullptr;
            int pending = 0;
            do
            {
                int msgq = 0;
                m = curl_multi_info_read(m_multi_handle, &msgq);
                if (m)
                    if (m->msg == CURLMSG_DONE)
                    {
                        CURL *e = m->easy_handle;
                        curl_multi_remove_handle(m_multi_handle, e);
                        curl_easy_cleanup(e);
                    }
                    else
                        pending++;
            } while (m);

            return pending;
        }

        void service_sessions()
        {
            // TODO: Check if resolution for known handles is fine
            // NOTE: Dont do this every call. Do it every x seconds
        }

        bool request(const char *data, size_t len, const char *url = nullptr)
        {
            auto handle = curl_easy_duphandle(m_curl);

            // You should set this beforehand with default_url
            if (url) [[unlikely]]
                curl_easy_setopt(handle, CURLOPT_URL, url);
                
            curl_easy_setopt(handle, CURLOPT_POSTFIELDSIZE, len);
            curl_easy_setopt(handle, CURLOPT_COPYPOSTFIELDS, data);
            curl_multi_add_handle(m_multi_handle, handle);

            return true;
        }
    };

}