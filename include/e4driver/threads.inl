
namespace e4driver
{
    enum Direction
    {
        Send = 0,
        Receive = 1
    };

    template <typename Module,
              typename Payload = long long,
              size_t Size = 10,
              typename QueueType = boost::lockfree::spsc_queue<Payload, boost::lockfree::capacity<Size>>>
    class thread_object_t
    {

    protected:
        using channel_t = std::array<QueueType, 2>;
        channel_t &m_channel; // Sender|Receiver

    public:
        thread_object_t(channel_t &chan) : m_channel(chan) {}
        thread_object_t() : m_channel(*new channel_t()) {}

        void initialize(auto config)
        {
            static_cast<Module *>(this)->initialize(config);
        }

        void start()
        {
            thread_obj = std::thread([this]
                                     { static_cast<Module *>(this)->run(); });
        }

        void join()
        {
            if (thread_obj.joinable())
            {
                thread_obj.join();
            }
        }

        channel_t &queues()
        {
            return m_channel;
        }

        long long nanosecond_timestamp()
        {
            auto now = std::chrono::time_point_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now());
            auto sinceEpoch = now.time_since_epoch();
            return sinceEpoch.count();
        }

    private:
        std::thread thread_obj;
    };

}