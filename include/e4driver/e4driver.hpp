#pragma once
#include <stdexcept>
#include <string>
#include <iostream>
#include <chrono>

// Thread and Ring Buffers
#include <boost/thread/thread.hpp>
#include <boost/lockfree/spsc_queue.hpp>
#include <boost/atomic.hpp>
#include <thread>

// REST client
#include <curl/curl.h>
#include <unistd.h>
#include <sys/epoll.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <fcntl.h>

#include <array>
#include <cassert>
#include <cerrno>
#include <cstring>
#include <iostream>
#include <string>

#include <ext/aixlog.hpp>

#include "listener.inl"
#include "restclient.inl"
#include "threads.inl"
#include "exceptions.inl"

