namespace e4driver
{
  namespace exceptions
  {
    class initialization_error : public std::exception
    {
    public:
      initialization_error(const std::string &message, int errorCode)
          : m_msg(message), m_code(errorCode) {}

      int error_code() const
      {
        return m_code;
      }

      const char *what() const noexcept override
      {
        return m_msg.c_str();
      }

    private:
      std::string m_msg;
      int m_code;
    };

  } // namespace exceptions

} // namespace e4driver
